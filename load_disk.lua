-- load floppy

-- Writes on monitor if available, otherwise writes to console
function advancedWrite(str, monitor)
    if monitor == false then
        print(str)
    else
        monitor.write(str)
    end
end

-- Gets the peripheral side based on passed type
function getPeripheral(type)
    local sides = {"top", "bottom", "left", "right"}
    for i = 0, #sides, 1 do
        if peripheral.getType(sides[i]) == type then
            return sides[i]
        end
    end

    return false
end

-- Side that the drive is on
local driveSide = getPeripheral("drive")

-- Get Drive Peripheral
local drive = peripheral.wrap(driveSide)

-- Get Monitor Peripheral
local monitorSide = getPeripheral("monitor")
local monitor = false
if not monitorSide then
    print("No Monitor Present")
else
    local monitor = peripheral.wrap(monitorSide)
end

-- Error Checking
if not drive then
    monitor.write("No Drive Present")
    return
end

if not drive.hasData(driveSide) then
    advancedWrite("No Data Present", monitor)
    return
end

term.clear()

if drive.hasAudio(driveSide) then
    advancedWrite("Audio Disk: " .. drive.getAudioTitle(driveSide), monitor)
    drive.playAudio(driveSide)
else
    advancedWrite(drive.getLabel(driveSide) .. " - " .. drive.getID(driveSide))
end


-- advanced miner

function returnToChest()
        if not os.loadAPI("minerapi") then
            print("Could not load \"minerapi\"")
            return false
        end
        turtle.turnLeft()
        turtle.turnLeft()
        clearInventory()
        while not turtle.detect() do
                minerapi.refuelTurtle()
                turtle.forward()
        end
        dropItemsToChest()
        return true
end

function returnToMine()
        if not os.loadAPI("minerapi") then
            print("Could not load \"minerapi\"")
            return false
        end
        minerapi.refuelTurtle()
        turtle.up()
        turtle.turnLeft()
        turtle.turnLeft()
        while not turtle.detect() do
                minerapi.refuelTurtle()
                turtle.forward()
        end
        turtle.down()
        return true
end

function clearInventory()
        for i = 4, 16, 1 do
                turtle.select(i)
                if turtle.compareTo(3) then
                        turtle.drop()
                        print("Dropping Slot: " .. i)
                end
        end
        turtle.select(1)
end

function dropItemsToChest()
        for i = 4, 16, 1 do
                turtle.select(i)
                turtle.drop()
        end
end

function mineRow()
        if not os.loadAPI("minerapi") then
            print("Could not load \"minerapi\"")
            return false
        end
        minerapi.refuelTurtle()
        turtle.up()

        if isLeft then
                for i = 1, 2, 1 do
                        minerapi.refuelTurtle()
                        while not turtle.forward() do
                                turtle.dig()
                        end
                        turtle.digDown()
                end
                turtle.turnLeft()
        else
                turtle.turnRight()
        end

        for i = 1, 20, 1 do
                minerapi.refuelTurtle()
                while not turtle.forward() do
                        turtle.dig()
                end
                turtle.digDown()
                if i == 1 or i == 10 then
                        turtle.select(2)
                        turtle.placeDown()
                end
        end

        turtle.turnLeft()
        turtle.turnLeft()
        turtle.select(2)
        turtle.placeDown()

        minerapi.refuelTurtle()
        for i = 1, 20, 1 do
                turtle.forward()
        end

        if isLeft then
                isLeft = not isLeft
                turtle.turnLeft()
        else
                isLeft = not isLeft
                turtle.turnRight()
        end

        turtle.down()
        return true
end

isLeft = true
while true do
        print("Mining Row")
        if not mineRow() then
            break
        end
        print("Dispensing to Chest")
        if not returnToChest() then
            break
        end
        print("Returning to Mine")
        if not returnToMine() then
            break
        end
end
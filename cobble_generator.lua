-- cobble generator

function addToChest()
	for i = 1, 16, 1 do
		turtle.select(i)
		if turtle.getItemCount(i) >= 64 then
			turtle.turnLeft()
			turtle.turnLeft()
			turtle.drop()
			turtle.turnLeft()
			turtle.turnLeft()
		end
	end
	turtle.select(1)
end

while true do
	if turtle.detect() then 
		turtle.dig()
		addToChest()
	end
end
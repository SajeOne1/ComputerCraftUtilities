-- man pages

function lines(str)
  local t = {}
  local function helper(line) table.insert(t, line) return "" end
  helper((str:gsub("(.-)\r?\n", helper)))
  return t
end

function readMan(str)
    local strLines = lines(str)
    write("Press any key until end")
    os.pullEvent("key")
    term.clear()
    for i = 0, #strLines, 1 do
        print(strLines[i])
        os.pullEvent("key")
    end
end

items = {}

items[0] = {}
items[0][0] = "build_frame"
items[0][1] = [[Build Frame - build_frame

Desc:
Fills in a square vertical frame with blocks.

How to use:
Build a vertical square frame of blocks, like a large nether portal with dimensions of your choice.
Place the turtle in one of the bottom corners of the frame, facing the opposite corner

Item Placement:
Slot 1: Fuel(Coal, Charcoal, Lava)
Slot 2-16: Block Materials(Used to fill frame; must be same material as frame)
]]

items[1] = {}
items[1][0] = "build_floor"
items[1][1] = [[Build Floor - build_floor

Desc:
Builds a roof off of a rectangular/square formation of walls

How to use:
Place the turtle one block to the left or right of a corner facing the oppsosite wall.

Item Placement:
Slot 1: Fuel(Coal, Charcoal, Lava)
Slot: 2-16: Block Materials]]

items[2] = {}
items[2][0] = "dig_floor"
items[2][1] = [[Dig Floor - dig_floor

Desc:
Digs the floor of a building down infinitely

How to use:
Place in the back left corner of a rectangular/square room
The turtle will turn right first when it hits the wall in-front of it.
It will then turn left the next time it  next until it reaches the end of the entire sqaure.
It will then drop down a level and continue.

Item Placement:
Slot 1: Fuel(Coal, Charcoal, Lava)
Slot 2: Torches
Slot 3: Comparator Block(Used to discard cobble or other materials it is mining)]]



local args = {...}

if #args >= 1 then
    for i = 0, #items, 1 do
        if items[i][0] == args[1] then
            --textutils.slowPrint(items[i][1], 5)
            readMan(items[i][1])
            return
        end
    end
    print("Could not find the program specified")
else
    print("You must specify a program")
end
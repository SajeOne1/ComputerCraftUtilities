-- Dig Down

function refuelTurtle()
	if turtle.getFuelLevel() < 1 then
		turtle.select(1)
		if not turtle.refuel() then
            return false
        end
		print("Turtle Refueled")
	end

    return true
end

while true do
    if turtle.detectDown() then
        turtle.digDown()
    else
        turtle.down()
    end

    if not refuelTurtle() then
        return
    end
    
    while not turtle.forward() do
        print("Turtle is obstructed")
    end

    if turtle.detect() then
        turtle.turnRight()
    end
end
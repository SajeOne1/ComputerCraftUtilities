-- tree chopper

function refuelTurtle()
	if turtle.getFuelLevel() < 1 then
		turtle.select(1)
		turtle.refuel(1)
		print("Turtle Refueled")
	end
end

function harvestTree()
	turtle.down()
	while turtle.detect() do
		turtle.dig()
		turtle.digUp()
		refuelTurtle()
		turtle.up()
	end

	while not turtle.detectDown() do
		refuelTurtle()
		turtle.down()
	end
end

function depositToChest()
	turtle.turnLeft()
	turtle.turnLeft()
	for i = 3, 16, 1 do
		turtle.select(i)
		turtle.drop()
	end
	turtle.turnLeft()
	turtle.turnLeft()
end

refuelTurtle()
while not turtle.up() do
	print("Something's in my way")
end

while true do
	if turtle.detect() then
		print("Harvesting Tree...")
		harvestTree() -- Chop down tree
		depositToChest()

		turtle.select(2) -- Select & Place Sapplings
		turtle.place()

		refuelTurtle()
		turtle.up()
	else
		print("Waiting for tree...")
	end
end
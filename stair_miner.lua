function refuelTurtle()
	if turtle.getFuelLevel() < 1 then
		turtle.select(1)
		turtle.refuel()
		print("Turtle Refueled")
	end
end

local canDig = true
while canDig do
	refuelTurtle()
	turtle.forward()
	if not turtle.digDown() then
		canDig = false
	else
		turtle.dig()
		turtle.up()
		turtle.dig()
		turtle.down()
		turtle.down()
		turtle.dig()
	end
end
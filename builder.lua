-- builder

function setMaterials()
	for i = 2, 16, 1 do
		turtle.select(i)
		if turtle.getItemCount(i) > 0 then
			print("Materials Found")
			return true
		end
	end
	return false
end

function refuelTurtle()
	if turtle.getFuelLevel() < 1 then
		turtle.select(1)
		turtle.refuel()
		print("Turtle Refueled")
	end
end

function getDimensions()
	while turtle.detectDown() do -- Detect down until he goes off edge to get width
		while not turtle.forward() do
				print("Get out of my way mob!")
			end
		width = width + 1
	end
	width = width - 2
	while not turtle.back() do
		print("Get out of my way mob!") -- Get him back on the platform
	end

	turtle.turnRight() -- Turn to get length

	while turtle.detectDown() do -- Detect down until he goes off edge to get length
		while not turtle.forward() do
				print("Get out of my way mob!")
			end
		length = length + 1
	end
	length = length - 2

	while not turtle.back() do
		print("Get out of my way mob!") -- Get him back on the platform
	end
	turtle.turnRight()

end

-- VARIABLE DECLARATIONS
width = 1
length = 1
isWidth = true
-- VARIABLE DECLARATIONS

-- Get Rectangle Dimensions
getDimensions()

while not turtle.up() do
	print("Get out of my way mob!")
end
setMaterials()
turtle.placeDown()

-- Place Items Infinitely
while true do
	for i = 1, 2, 1 do -- Do it twice to complete full square; 2*length & 2*width
		for i = 1, width, 1 do -- Place for Width
			refuelTurtle()
			while not turtle.forward() do
				print("Get out of my way mob!")
			end
			if not setMaterials() then
				return
			end
			turtle.placeDown()
		end
		turtle.turnRight()
		for i = 1, length, 1 do -- Place for Length
			refuelTurtle()
			while not turtle.forward() do
				print("Get out of my way mob!")
			end
			if not setMaterials() then
				return
			end
			turtle.placeDown()
		end
		turtle.turnRight()
	end
	while not turtle.up() do
		print("Get out of my way mob!")
	end
end
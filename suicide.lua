-- suicide turtle
function refuelTurtle()
	if turtle.getFuelLevel() < 1 then
		turtle.select(1) -- fuel
		turtle.refuel(1)
		print("Turtle Refueled")
	end
end

while not turtle.detect() do
	refuelTurtle()
	turtle.forward()
end
turtle.back()
turtle.select(4) -- cobble
turtle.place()
turtle.up()
turtle.select(2) -- redstone torch
turtle.place()
turtle.up()
turtle.select(3)
turtle.place()

turtle.turnLeft()
turtle.turnLeft()

for i = 1, 50, 1 do
	if not turtle.detect() then
		refuelTurtle()
		turtle.forward()
	else
		while not turtle.detectDown() do
			refuelTurtle()
			turtle.down()
		end
	end
end
-- Quarry

function movePath(num)
    for i = 1; num, 1 do
        turtle.dig()
        minerapi.refuelTurtle()
        while not turtle.forward() do
            print("Turtle is obstructed")
        end
end

if not os.loadAPI("minerapi") then
    print("Dependency \"minerapi\" not met")
    return false
end

local arg = { ... }

local num = tonumber(arg[1])

if num == nil or (num - math.floor(num)) ~= 0 then
    print("Invalid number given")
    return
end

while true do
    for i = 1, 4, 1 do
        movePath()
        turtle.turnLeft()
    end

    minerapi.refuelTurtle()
    while not turtle.down() do
        turtle.digDown()
    end
end

local isLastRow = false
function setMaterials()
	for i = 2, 16, 1 do
		turtle.select(i)
		if turtle.compareDown() then
			print("materials found")
			return i
		end
	end
	return -1
end

function refuelTurtle()
	if turtle.getFuelLevel() < 1 then
		turtle.select(1)
		turtle.refuel()
		print("Turtle Refueled")
	end
end

function buildLastRow()
	while turtle.back() do
		setMaterials()
		turtle.place()
		refuelTurtle()
	end
	turtle.turnLeft()
	turtle.back()
	turtle.place()
	climbDown()
end

function climbDown()
	while not turtle.detectDown() do
		turtle.down()
	end
end
	

setMaterials()
turtle.up()
turtle.placeDown()

i = 1

while i ~= -1 do
	i = setMaterials()
	if i == -1 then
		return
	end
	turtle.select(i);
	if turtle.detect() and turtle.detectUp() then
		if not isLastRow then
			isLastRow = true
			buildLastRow()
		else
			break
		end
		else if turtle.detect() then
			turtle.up()
			turtle.turnLeft()
			turtle.turnLeft()
			turtle.placeDown()
		else
			refuelTurtle()
			turtle.select(setMaterials())
			turtle.forward()
			turtle.placeDown()
		end
	end
end
-- place stairs
function setMaterials()
	for i = 2, 16, 1 do
		turtle.select(i)
		if turtle.getItemCount(i) > 0 then
			print("materials found")
			return true
		end
	end
	return false
end

function refuelTurtle()
	if turtle.getFuelLevel() < 1 then
		turtle.select(1)
		turtle.refuel(1)
		print("Turtle Refueled")
	end
end

turtle.turnLeft()
turtle.turnLeft()

refuelTurtle()
turtle.back()
setMaterials()
turtle.down()

while true do
	refuelTurtle()
	turtle.back()
	setMaterials()
	if not turtle.place() then
		break
	else
	end
	turtle.down()
end
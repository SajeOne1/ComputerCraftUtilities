-- farmer

function refuelTurtle()
	if turtle.getFuelLevel() < 1 then
		turtle.select(1)
		if not turtle.refuel() then
            return false
        end
		print("Turtle Refueled")
	end

    return true
end


-- return number of blocks travelled
function harvestRow()
    count = 0
    while not turtle.detect() do
        refuelTurtle()
        while not turtle.forward() do
            print("Turtle is obstructed")
        end

        count = count + 1
        turtle.digDown()
    end

    return count
end


function tillRow(num)
    for i = 1, num, 1 do
        turtle.digDown()
        turtle.select(2)
        turtle.placeDown()

        refuelTurtle()
        while not turtle.forward() do
            print("Turtle is obstructed")
        end
    end
end

function getToNextRow()
    turtle.turnLeft()
    while true do
        if turtle.detect() then
            return true
        end

        refuelTurtle()
        turtle.forward()
        turtle.turnLeft()
        turtle.forward()
        if turtle.detectDown() then
            turtle.back()
            break
        else
            turtle.down()
            if turtle.detectDown() then
                turtle.up()
                turtle.back()
                break
            else
                turtle.up()
                turtle.back()
                turtle.turnRight()
            end
        end
    end

    return false
end

function returnAndDeposit()
    turtle.turnLeft()
    turtle.turnLeft()

    refuelTurtle()
    turtle.down()

    while not turtle.detect() do
        refuelTurtle()
        turtle.forward()
    end

    for i = 3, 16, 1 do
        turtle.select(i)
        turtle.drop()
    end

    turtle.turnRight()
end


refuelTurtle()
turtle.up()

while true do
    local count = harvestRow()

    turtle.turnLeft()
    turtle.turnLeft()

    tillRow(count)
    if getToNextRow() then
        print("Finished")
        break
    end
end

returnAndDeposit()
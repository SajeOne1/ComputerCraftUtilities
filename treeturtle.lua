function failsafe()
    turtle.select(1)
    if not turtle.compareTo(14) then
        print("No more Saplings")
        print("Press any button to shutdown computer...")
        os.pullEvent("key")
        stop()
    end
    turtle.select(2)
    if not turtle.compareTo(15) then
        print("No more Bone Meal")
        print("Press any button to shutdown computer...")
        os.pullEvent("key")
        stop()
    end
    turtle.select(3)
    if not turtle.compareTo(16) then
        print("No more fuel")
        print("Press any button to shutdown computer...")
        os.pullEvent("key")
        stop()
    end
end
function stop()
    os.shutdown()
end
function chop()
    while turtle.detect() do
        turtle.dig()
        turtle.digUp()
        miner.refuelTurtle()
        turtle.up()
    end
end

Bonemealcount = 5
while true do
    if not os.loadAPI("minerapi") then
        print("Could not find \"minerapi\"")
        return
    end
    failsafe()
    while turtle.getFuelLevel() <= 20 do
        turtle.select(3)
        turtle.refuel(1)
    end
    turtle.select(1) --Selects and places sapling.
    turtle.place()
    turtle.select(2) --Selects bonemeal.
    for i = 1, Bonemealcount do --Places bonemeal on sapling.
        turtle.place()
    end
    while turtle.detect() do --Wood chopping algorithm.
        chop()
    end
    while not turtle.detectDown() do
        turtle.down()
    end
end
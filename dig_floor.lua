-- dig floor
function refuelTurtle()
	if turtle.getFuelLevel() < 1 then
		turtle.select(1)
		turtle.refuel(1)
		print("Turtle Refueled")
	end
end

function clearInventory()
	for i = 4, 16, 1 do
		turtle.select(i)
		if turtle.compareTo(3) then
			turtle.drop()
			print("Dropping Slot: " .. i)
		end
	end
	turtle.select(1)
end

function endOfRow()
	turtle.digDown()
	clearInventory()
	if isRight then
		turtle.turnRight()
		refuelTurtle()
		if turtle.forward() then
			isRight = not isRight
			turtle.turnRight()
		else
			turtle.turnRight() -- 1b6dgVXb
			turtle.down()
		end
	else
		turtle.turnLeft()
		refuelTurtle()
		if turtle.forward() then
			isRight = not isRight
			turtle.turnLeft()
		else
			turtle.turnLeft()
			refuelTurtle()
			turtle.down()
		end
	end
end

isRight = true
local blockCount = 0
while true do
	while not turtle.detect() do
		turtle.digDown()
		refuelTurtle()
		if blockCount == 24 then
			blockCount = 0
			turtle.select(2)
			turtle.placeDown()
			turtle.forward()
		else
			turtle.forward()
			blockCount = blockCount + 1
		end
	end

	turtle.up()
	if not turtle.detect() then
		turtle.down()
		turtle.dig()
	else
		turtle.down()
		endOfRow()
	end
end
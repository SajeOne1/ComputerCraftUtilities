-- build floor
function setMaterials()
	for i = 2, 16, 1 do
		turtle.select(i)
		if turtle.getItemCount(i) > 0 then
			print("materials found")
			return true
		end
	end
	return false
end

function refuelTurtle()
	if turtle.getFuelLevel() < 1 then
		turtle.select(1)
		turtle.refuel()
		print("Turtle Refueled")
	end
end

local isRightTurn = true
while true do
	refuelTurtle()
	while not turtle.forward() do
		print("Something is in the way of the turtle!\n")
	end
	if turtle.detectDown() then
		break
	end
	while not turtle.detectDown() do
		if not setMaterials() then
			refuelTurtle()
			turtle.back()
			return
		end
		turtle.placeDown()
		refuelTurtle()
		while not turtle.forward() do
		end
	end
	if isRightTurn then

		isRightTurn = not isRightTurn
		turtle.turnRight()
		refuelTurtle()
		while not turtle.forward() do
		end
		turtle.turnRight()
	else
		isRightTurn = not isRightTurn
		turtle.turnLeft()
		refuelTurtle()
		while not turtle.forward() do
		end
		turtle.turnLeft()
	end
end
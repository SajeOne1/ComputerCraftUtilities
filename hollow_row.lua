-- Hollow Row - Hollows a row

function refuelTurtle()
    if turtle.getFuelLevel() < 1 then
        turtle.select(1)
        turtle.refuel(1)
    end
end

while turtle.detectUp() do
    turtle.digUp()
end

turtle.up()

local count = -1

while true do
    while turtle.detect() do
        turtle.dig()
        sleep(1)
    end
    refuelTurtle()
    turtle.forward()
    count = count + 1
    turtle.digDown()
    turtle.digUp()
    if not turtle.detect() then
        turtle.back()
        turtle.select(2)
        turtle.place()
        turtle.up()
        if not turtle.detect() then
            turtle.place()
        end
        turtle.down()
        turtle.digDown()
        turtle.down()
        if not turtle.detect() then
            turtle.place()
        end
        break
    end
end

turtle.turnLeft()
turtle.turnLeft()

for i = 1, count, 1 do
    while not turtle.forward() do
    end
    refuelTurtle()
end

turtle.turnLeft()
turtle.turnLeft()
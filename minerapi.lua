-- miner api

function refuelTurtle()
	if turtle.getFuelLevel() < 1 then
		turtle.select(1)
		if not turtle.refuel(1) then
            return false
        end
		print("Turtle Refueled")
	end

    return true
end
-- simple miner

function refuelTurtle()
	if turtle.getFuelLevel() < 1 then
		turtle.select(1) -- fuel
		turtle.refuel(1)
		print("Turtle Refueled")
	end
end

refuelTurtle()
turtle.up()

for i = 1, 20, 1 do
	refuelTurtle()
	while not turtle.forward() do
		turtle.dig()
	end
	turtle.digDown()
	if i == 1 or i == 10 then
		turtle.select(2)
		turtle.placeDown()
	end
end

turtle.turnLeft()
turtle.turnLeft()

turtle.select(2)
turtle.placeDown()

for i = 1, 20, 1 do
	refuelTurtle()
	turtle.forward()
end

turtle.turnLeft()

for i = 1, 2, 1 do
	while not turtle.forward() do
		turtle.dig()
	end
	turtle.digDown()
end

turtle.turnLeft()
turtle.down()
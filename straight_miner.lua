-- Straight Miner

function mineForward()
    if not os.loadAPI("minerapi") then
        print("Could not load \"minerapi\"")
        return false
    end
    
    while not turtle.detect() do
        minerapi.refuelTurtle()
        turtle.forward()
    end

    for i = 0, 20, 1 do
        while turtle.detect() do
            turtle.dig()
            os.sleep(1)
        end

        minerapi.refuelTurtle()
        turtle.forward()
        turtle.digDown()

        if i == 10 or i == 20 then
            turtle.select(2)
            turtle.placeDown()
            turtle.select(1)
        end
    end
end

function returnToChest()
    while not turtle.detect() do
        minerapi.refuelTurtle()
        turtle.forward()
    end

    for i = 4, 16, 1 do
        turtle.select(i)
        turtle.drop()
    end
end

if not os.loadAPI("minerapi") then
    print("Could not load \"minerapi\"")
    return false
end

minerapi.refuelTurtle()
turtle.up()

while true do
    mineForward()

    turtle.turnLeft()
    turtle.turnLeft()

    returnToChest()

    turtle.turnLeft()
    turtle.turnLeft()
end